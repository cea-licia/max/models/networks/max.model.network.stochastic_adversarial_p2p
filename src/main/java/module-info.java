open module max.model.network.stochastic_adversarial_p2p {
    // Exported packages
    exports max.model.network.stochastic_adversarial_p2p.action;
    exports max.model.network.stochastic_adversarial_p2p.action.config;
    exports max.model.network.stochastic_adversarial_p2p.action.message;
    exports max.model.network.stochastic_adversarial_p2p.action.patterns;
    exports max.model.network.stochastic_adversarial_p2p.agent;
    exports max.model.network.stochastic_adversarial_p2p.context;
    exports max.model.network.stochastic_adversarial_p2p.context.delay;
    exports max.model.network.stochastic_adversarial_p2p.context.filter;
    exports max.model.network.stochastic_adversarial_p2p.context.handler;
    exports max.model.network.stochastic_adversarial_p2p.env;
    exports max.model.network.stochastic_adversarial_p2p.msgcount;
    exports max.model.network.stochastic_adversarial_p2p.msglog;
    exports max.model.network.stochastic_adversarial_p2p.role;

    // Dependencies
    requires java.logging;
    requires java.desktop;
    requires org.apache.commons.lang3;

    requires madkit;      // Automatic

    requires max.core;
    requires max.datatype.com;

    // Optional dependency (required for tests)
    requires static org.junit.jupiter.api;

}