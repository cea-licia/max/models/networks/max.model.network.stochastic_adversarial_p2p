/*********************************************************************
 * Copyright (c) 2023 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/
package max.model.network.stochastic_adversarial_p2p.action.message;

import madkit.kernel.AgentAddress;
import max.core.action.Action;
import max.datatype.com.Message;
import max.model.network.stochastic_adversarial_p2p.agent.StochasticPeerAgent;
import max.model.network.stochastic_adversarial_p2p.env.StochasticP2PEnvironment;
import max.model.network.stochastic_adversarial_p2p.role.RStochasticP2PEnvironment;

import java.util.Collections;
import java.util.Vector;

/**
 * This action is triggered by {@link AcEmitMessageTowardsNetwork}:
 * - if it is not censored at the output
 * - and after the involved output delay has passed
 *
 * It triggers the execution of several {@link AcForwardMessageToRecipient} actions,
 * one per intended recipient of the transmitted message
 *
 * @author Erwan Mahe
 */
public class AcDispatchMessageOverNetwork extends Action<StochasticP2PEnvironment> {

	private final Message<AgentAddress, ?> messageToDispatch;

	/**
	 * Creates a new instance of {@link AcDispatchMessageOverNetwork}.
	 *
	 * @param owner the owner of the action (is a {@link StochasticP2PEnvironment})
	 * @param messageToDispatch the message
	 */
	public AcDispatchMessageOverNetwork(StochasticP2PEnvironment owner,
										Message<AgentAddress, ?> messageToDispatch) {
		super(owner.getName(), RStochasticP2PEnvironment.class, owner);
		this.messageToDispatch = messageToDispatch;
	}

	@Override
	public void execute() {
		getOwner().getLogger().finer("delivering to network message:\n" + this.messageToDispatch + "\n");
		// ***
		// retrieve list of peers
		// ***
		for (AgentAddress receiverAddress : this.messageToDispatch.getReceivers()) {
			Vector<AgentAddress> target = new Vector<>(Collections.singletonList(receiverAddress));
			final Message<AgentAddress, ?> dispatch = this.getOwner().copyWithNewReceivers(this.messageToDispatch, target, false);
			// ***
			// retrieve target agent
			StochasticPeerAgent peer = (StochasticPeerAgent) receiverAddress.getAgent();
			// ***
			Action forwardingAction = new AcForwardMessageToRecipient(this.getEnvironment(), peer, dispatch);
			forwardingAction.execute();
		}
		getOwner().getLogger().finer("delivered message to network");
	}

	@Override
	public AcDispatchMessageOverNetwork copy() {
		return new AcDispatchMessageOverNetwork(getOwner(), this.messageToDispatch);
	}

}
