/*********************************************************************
 * Copyright (c) 2023 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/

package max.model.network.stochastic_adversarial_p2p.action.message;

import madkit.kernel.AgentAddress;
import max.core.action.Action;
import max.core.scheduling.ActionActivator;
import max.core.scheduling.ActivationScheduleFactory;
import max.datatype.com.Message;
import max.model.network.stochastic_adversarial_p2p.agent.StochasticPeerAgent;
import max.model.network.stochastic_adversarial_p2p.context.StochasticP2PContext;
import max.model.network.stochastic_adversarial_p2p.context.delay.DelaySpecification;
import max.model.network.stochastic_adversarial_p2p.context.filter.P2PMessageFilter;
import max.model.network.stochastic_adversarial_p2p.env.StochasticP2PEnvironment;
import max.model.network.stochastic_adversarial_p2p.role.RStochasticNetworkPeer;
import org.apache.commons.lang3.tuple.Pair;

import java.math.BigDecimal;
import java.util.Map;
import java.util.Vector;

/**
 * This action is triggered by {@link AcDispatchMessageOverNetwork}.
 * The action retrieves the context of the agent (i.e., and instance of {@link StochasticP2PContext}
 * And, depending on the context, the message may be:
 * - redirected towards listeners/subscribers/spies
 * - censored (i.e., not received)
 * - delayed
 *
 * If the message reception is not censored, it schedules a {@link AcHandleMessageOnReception} action after the delays.
 *
 *
 * @author Erwan Mahe
 */
public class AcForwardMessageToRecipient<A extends StochasticPeerAgent> extends Action<A> {

    private final Message<AgentAddress, ?> messageToForward;

    /**
     * Creates a new instance of {@link AcEmitMessageTowardsNetwork}.
     *
     * @param environment where the action is executed
     * @param owner the owner of the action
     * @param messageToForward the message
     */
    public AcForwardMessageToRecipient(String environment, A owner, Message<AgentAddress, ?> messageToForward) {
        super(environment, RStochasticNetworkPeer.class, owner);
        this.messageToForward = messageToForward;
    }

    @Override
    public void execute() {
        getOwner().getLogger().info( "message of type " + this.messageToForward.getType() + " being forwarded to " + this.getOwner().getName());
        // ***
        // retrieving context and environment
        final StochasticP2PContext context = (StochasticP2PContext) this.getOwner().getContext(getEnvironment());
        StochasticP2PEnvironment environment = (StochasticP2PEnvironment) context.getEnvironment();
        // ***
        // potentially redirecting received message towards listeners
        for (Map.Entry<String, Pair<P2PMessageFilter, Vector<AgentAddress>>> entry : context.inputRedirects.entrySet()) {
            final String key = entry.getKey();
            final Pair<P2PMessageFilter, Vector<AgentAddress>> value = entry.getValue();
            final P2PMessageFilter filter = value.getLeft();
            if (filter.goes_through_the_filter(this.messageToForward)) {
                final Vector<AgentAddress> redirectRecipients = value.getRight();
                getOwner().getLogger().info("for reason : " + key + " , redirecting towards " + redirectRecipients);
                Message copiedMessage = environment.copyWithNewReceivers(this.messageToForward, redirectRecipients, true);
                (new AcDispatchMessageOverNetwork(environment,copiedMessage)).execute();
            }
        }
        // ***
        // potentially censoring the message (i.e., not receiving it)
        for (Map.Entry<String, P2PMessageFilter> entry : context.inputCensors.entrySet()) {
            final String key = entry.getKey();
            final P2PMessageFilter filter = entry.getValue();
            if (filter.goes_through_the_filter(this.messageToForward)) {
                getOwner().getLogger().info("for reason : " + key + " , censoring message");
                return;
            }
        }
        // ***
        // potentially delaying the message
        int sumOfDelays = 0;
        for (Map.Entry<String, Pair<P2PMessageFilter, DelaySpecification>> entry : context.inputDelays.entrySet()) {
            final String key = entry.getKey();
            final Pair<P2PMessageFilter, DelaySpecification> value = entry.getValue();
            final P2PMessageFilter filter = value.getLeft();
            if (filter.goes_through_the_filter(this.messageToForward)) {
                final DelaySpecification delaySpecification = value.getRight();
                int concreteDelay = delaySpecification.get_concrete_delay();
                getOwner().getLogger().info("for reason : " + key + " , delaying message by " + concreteDelay);
                sumOfDelays += concreteDelay;
            }
        }
        // ***
        // scheduling delivery to network
        Action messageReceptionAction = new AcHandleMessageOnReception<A>(getEnvironment(), getOwner(), this.messageToForward);
        if (sumOfDelays > 0) {
            getOwner().getLogger().info("delaying message reception by a total of " + sumOfDelays);
            final var nextTime = getOwner().getSimulationTime().getCurrentTick().add(BigDecimal.valueOf(sumOfDelays));
            context.getOwner()
                    .schedule(new ActionActivator<>(ActivationScheduleFactory.createOneTime(nextTime),messageReceptionAction));
        } else {
            messageReceptionAction.execute();
        }
    }

    @Override
    public <T extends Action<A>> T copy() {
        return (T) new AcForwardMessageToRecipient<>(getEnvironment(), getOwner(), this.messageToForward);
    }



}