/*********************************************************************
 * Copyright (c) 2023 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/

package max.model.network.stochastic_adversarial_p2p.action;


import max.core.action.Action;
import max.model.network.stochastic_adversarial_p2p.env.*;
import max.model.network.stochastic_adversarial_p2p.msgcount.MessageCounterSingleton;
import max.model.network.stochastic_adversarial_p2p.msgcount.MsgCountNature;
import max.model.network.stochastic_adversarial_p2p.role.RStochasticP2PEnvironment;
import org.apache.commons.lang3.tuple.Pair;


import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;


/**
 * Action executed by the P2P environment.
 * Its aim is to display the current messages count.
 *
 * @author Erwan Mahe
 */
public class AcCollectMessageCount extends Action<StochasticP2PEnvironment> {

    private final Optional<String> outputFilePath;

    public AcCollectMessageCount(String environment,
                            StochasticP2PEnvironment owner,
                                 Optional<String> outputFilePath) {
        super(environment, RStochasticP2PEnvironment.class, owner);
        this.outputFilePath = outputFilePath;
    }

    @Override
    public void execute() {
        List<String> toWrite = new ArrayList<>();
        MessageCounterSingleton msgCountSingleton = MessageCounterSingleton.getInstance();
        for (String environmentName : msgCountSingleton.countPerEnvironment.keySet()) {
            HashMap<String, HashMap<MsgCountNature,Integer>> countForEnv = msgCountSingleton.countPerEnvironment.get(environmentName);
            for (String msgCountKey : countForEnv.keySet()) {
                HashMap<MsgCountNature,Integer> countForKey = countForEnv.get(msgCountKey);
                getOwner().getLogger().info("In environment " + environmentName +
                        " observed for messages matching " + msgCountKey + " " +
                        countForKey.get(MsgCountNature.UniqueEmission) + " unique emissions " +
                        countForKey.get(MsgCountNature.BroadcastEmission) + " broadcast emissions and " +
                                countForKey.get(MsgCountNature.Reception) + " receptions"
                        );
                toWrite.add(environmentName + ";" + msgCountKey + ";" + countForKey.get(MsgCountNature.UniqueEmission) + ";" + countForKey.get(MsgCountNature.BroadcastEmission) + ";" + countForKey.get(MsgCountNature.Reception));
            }
        }
        if (this.outputFilePath.isPresent()) {
            String outputFP = this.outputFilePath.get();
            try {
                FileWriter fw = new FileWriter(outputFP, false);
                BufferedWriter bw = new BufferedWriter(fw);
                bw.write( String.join("\n", toWrite) );
                bw.newLine();
                bw.close();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }

    @Override
    public <T extends Action<StochasticP2PEnvironment>> T copy() {
        return (T) new AcCollectMessageCount(
                getEnvironment(),
                getOwner(),
                this.outputFilePath);
    }

}
