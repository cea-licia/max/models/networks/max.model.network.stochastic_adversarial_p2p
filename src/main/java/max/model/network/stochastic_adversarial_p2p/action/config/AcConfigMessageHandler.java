/*********************************************************************
 * Copyright (c) 2023 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/
package max.model.network.stochastic_adversarial_p2p.action.config;

import max.core.action.Action;
import max.model.network.stochastic_adversarial_p2p.agent.StochasticPeerAgent;
import max.model.network.stochastic_adversarial_p2p.context.StochasticP2PContext;
import max.model.network.stochastic_adversarial_p2p.context.handler.OnReceiveMessageHandler;
import max.model.network.stochastic_adversarial_p2p.role.RStochasticNetworkPeer;

/**
 * Action triggered whenever a peer is reconfigured so that
 * a new message hander is either added or replaces a previous one.
 *
 * @author Erwan Mahe
 */
public class AcConfigMessageHandler<A extends StochasticPeerAgent> extends Action<A> {

    private final String messageType;

    private final OnReceiveMessageHandler handler;

    /**
     * Default constructor.
     *
     * @param environment
     * @param owner
     * @param messageType
     * @param handler
     */
    public AcConfigMessageHandler(final String environment, final A owner, String messageType, OnReceiveMessageHandler handler) {
        super(environment, RStochasticNetworkPeer.class, owner);
        this.messageType = messageType;
        this.handler = handler;
    }

    @Override
    public void execute() {
        final StochasticP2PContext context = (StochasticP2PContext) getOwner().getContext(getEnvironment());
        context.setOnReceiveHandlerForMessageType(this.messageType,this.handler);
    }

    @Override
    public <T extends Action<A>> T copy() {
        return (T) new AcConfigMessageHandler<>(getEnvironment(), getOwner(), this.messageType, this.handler);
    }

}
