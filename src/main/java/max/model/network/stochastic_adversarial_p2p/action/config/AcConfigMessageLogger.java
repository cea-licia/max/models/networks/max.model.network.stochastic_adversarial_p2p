/*********************************************************************
 * Copyright (c) 2023 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/
package max.model.network.stochastic_adversarial_p2p.action.config;

import max.core.action.Action;
import max.model.network.stochastic_adversarial_p2p.agent.StochasticPeerAgent;
import max.model.network.stochastic_adversarial_p2p.context.StochasticP2PContext;
import max.model.network.stochastic_adversarial_p2p.role.RStochasticNetworkPeer;
import max.model.network.stochastic_adversarial_p2p.msglog.AbstractMessageLogWriter;
import org.apache.commons.lang3.tuple.Pair;

import java.util.Optional;

/**
 * Action triggered whenever a peer is reconfigured so that
 * it will log send and received messages to a dedicated file.
 *
 * @author Erwan Mahe
 */
public class AcConfigMessageLogger<A extends StochasticPeerAgent> extends Action<A> {

    private final String pathPrefix;
    private final AbstractMessageLogWriter messageLogWriter;

    public AcConfigMessageLogger(final String environment, final A owner, String pathPrefix, AbstractMessageLogWriter messageLogWriter) {
        super(environment, RStochasticNetworkPeer.class, owner);
        this.pathPrefix = pathPrefix;
        this.messageLogWriter = messageLogWriter;
    }

    @Override
    public void execute() {
        final StochasticP2PContext context = (StochasticP2PContext) getOwner().getContext(getEnvironment());
        context.messageLogWriter = Optional.of(Pair.of(this.pathPrefix,this.messageLogWriter));
    }

    @Override
    public <T extends Action<A>> T copy() {
        return (T) new AcConfigMessageLogger<>(getEnvironment(), getOwner(),this.pathPrefix, this.messageLogWriter);
    }

}
