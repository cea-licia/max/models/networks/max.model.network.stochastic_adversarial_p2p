/*********************************************************************
 * Copyright (c) 2023 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/
package max.model.network.stochastic_adversarial_p2p.action.config;

import max.core.action.Action;
import max.model.network.stochastic_adversarial_p2p.agent.StochasticPeerAgent;
import max.model.network.stochastic_adversarial_p2p.context.StochasticP2PContext;
import max.model.network.stochastic_adversarial_p2p.context.delay.DelaySpecification;
import max.model.network.stochastic_adversarial_p2p.context.filter.P2PMessageFilter;
import max.model.network.stochastic_adversarial_p2p.role.RStochasticNetworkPeer;
import org.apache.commons.lang3.tuple.Pair;

/**
 * Action triggered whenever a peer is reconfigured so that
 * a new communication delay specification is either added or replaces a previous one.
 *
 * @author Erwan Mahe
 */
public class AcAddCommunicationDelay<A extends StochasticPeerAgent> extends Action<A> {

    private final String inputOrOutput;
    private final String delayCause;

    private final P2PMessageFilter messageFilter;

    private final DelaySpecification delaySpecification;

    /**
     * Default constructor.
     */
    public AcAddCommunicationDelay(final String environment, final A owner, String inputOrOutput, String delayCause, P2PMessageFilter messageFilter, DelaySpecification delaySpecification) {
        super(environment, RStochasticNetworkPeer.class, owner);
        this.inputOrOutput = inputOrOutput;
        this.delayCause = delayCause;
        this.messageFilter = messageFilter;
        this.delaySpecification = delaySpecification;
    }

    @Override
    public void execute() {
        final StochasticP2PContext context = (StochasticP2PContext) getOwner().getContext(getEnvironment());
        if (this.inputOrOutput == "INPUT") {
            if (context.inputDelays.containsKey(this.delayCause)) {
                this.getOwner().getLogger().warning("replacing previous input delay for cause : " + this.delayCause);
            } else {
                this.getOwner().getLogger().info("setting input delay for cause : " + this.delayCause);
            }
            context.inputDelays.put(this.delayCause, Pair.of(this.messageFilter,this.delaySpecification));
        } else if (this.inputOrOutput == "OUTPUT") {
            if (context.outputDelays.containsKey(this.delayCause)) {
                this.getOwner().getLogger().warning("replacing previous output delay for cause : " + this.delayCause);
            } else {
                this.getOwner().getLogger().info("setting output delay for cause : " + this.delayCause);
            }
            context.outputDelays.put(this.delayCause, Pair.of(this.messageFilter,this.delaySpecification));
        } else {
            this.getOwner().getLogger().severe("trying to add delay neither for INPUT nor for OUTPUT : " + this.inputOrOutput);
        }
    }

    @Override
    public <T extends Action<A>> T copy() {
        return (T) new AcAddCommunicationDelay<>(getEnvironment(), getOwner(), this.inputOrOutput, this.delayCause, this.messageFilter, this.delaySpecification);
    }

}
