/*********************************************************************
 * Copyright (c) 2023 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/
package max.model.network.stochastic_adversarial_p2p.action.message;

import madkit.kernel.AgentAddress;
import max.core.action.Action;
import max.datatype.com.Message;
import max.model.network.stochastic_adversarial_p2p.agent.StochasticPeerAgent;
import max.model.network.stochastic_adversarial_p2p.context.StochasticP2PContext;
import max.model.network.stochastic_adversarial_p2p.context.handler.OnReceiveMessageHandler;
import max.model.network.stochastic_adversarial_p2p.msgcount.MessageCounterSingleton;
import max.model.network.stochastic_adversarial_p2p.msgcount.MsgCountNature;
import max.model.network.stochastic_adversarial_p2p.msglog.MessageLoggerSingleton;
import max.model.network.stochastic_adversarial_p2p.role.RStochasticNetworkPeer;

/**
 * Action triggered whenever a peer receives a message.
 *
 * @author Erwan Mahe
 */
public class AcHandleMessageOnReception<A extends StochasticPeerAgent> extends Action<A> {

    private final Message<AgentAddress, ?> messageToHandle;


    public AcHandleMessageOnReception(final String environment, final A owner, Message<AgentAddress, ?> messageToHandle) {
        super(environment, RStochasticNetworkPeer.class, owner);
        this.messageToHandle = messageToHandle;
    }

    @Override
    public void execute() {
        getOwner().getLogger().info( "message of type " + this.messageToHandle.getType() + " being received by " + this.getOwner().getName());
        // counting
        if (this.messageToHandle.getCountKey() != null) {
            MessageCounterSingleton.getInstance()
                    .registerMessageCount(
                            this.getEnvironment(),
                            this.messageToHandle.getCountKey(),
                            MsgCountNature.Reception,
                            1);
        }
        // logging
        final StochasticP2PContext context = (StochasticP2PContext) this.getOwner().getContext(getEnvironment());
        context.messageLogWriter.ifPresent(
                got -> {
                    String pathPrefix = got.getLeft();
                    MessageLoggerSingleton.getInstance()
                            .logMessage(
                                    pathPrefix,
                                    getOwner().getName(),
                                    got.getRight().messageToString(this.messageToHandle),
                                    true);
                }
        );
        // ***
        OnReceiveMessageHandler handler = context.getOnReceiveHandlerForMessageType(this.messageToHandle.getType());
        handler.handle(context, this.messageToHandle);
    }

    @Override
    public <T extends Action<A>> T copy() {
        return (T) new AcHandleMessageOnReception<>(getEnvironment(), getOwner(), this.messageToHandle);
    }

}
