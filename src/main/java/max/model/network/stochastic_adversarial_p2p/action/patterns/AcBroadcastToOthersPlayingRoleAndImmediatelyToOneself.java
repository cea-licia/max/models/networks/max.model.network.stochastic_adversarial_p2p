/*********************************************************************
 * Copyright (c) 2023 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/

package max.model.network.stochastic_adversarial_p2p.action.patterns;

import madkit.kernel.AgentAddress;
import max.core.action.Action;
import max.core.role.Role;
import max.datatype.com.Message;
import max.model.network.stochastic_adversarial_p2p.action.message.AcEmitMessageTowardsNetwork;
import max.model.network.stochastic_adversarial_p2p.action.message.AcHandleMessageOnReception;
import max.model.network.stochastic_adversarial_p2p.agent.StochasticPeerAgent;
import max.model.network.stochastic_adversarial_p2p.context.StochasticP2PContext;
import max.model.network.stochastic_adversarial_p2p.role.RStochasticNetworkPeer;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Action executed whenever a P2P peer uses a primitive broadcast which corresponds to:
 * - the (n-1) other broadcast peers potentially receiving the message after some delays (as per the P2P model)
 * - itself immediately receiving the message
 *
 * Here the selection of the targets is made according to a role that they must play.
 *
 * @author Erwan Mahe
 */
public class AcBroadcastToOthersPlayingRoleAndImmediatelyToOneself<T_payload,A extends StochasticPeerAgent> extends Action<A> {

    private final Class<? extends Role> rolePlayedByParticipants;
    private final String messageType;
    private final String messageCountKey;

    private final T_payload messagePayload;

    /**
     * A set of agent names to which not to broadcast the message.
     * This can then be used to perform equivocation.
     * **/
    private final HashSet<String> excludedTargetsByName;

    public AcBroadcastToOthersPlayingRoleAndImmediatelyToOneself(String environmentName,
                                                          A owner,
                                                          T_payload messagePayload,
                                                          String messageType,
                                                          String messageCountKey,
                                                          Class<? extends Role> rolePlayedByParticipants,
                                                                 HashSet<String> excludedTargetsByName) {
        super(environmentName, RStochasticNetworkPeer.class, owner);
        this.messagePayload = messagePayload;
        this.messageType = messageType;
        this.messageCountKey = messageCountKey;
        this.rolePlayedByParticipants = rolePlayedByParticipants;
        this.excludedTargetsByName = excludedTargetsByName;
    }

    @Override
    public void execute() {
        StochasticP2PContext context = (StochasticP2PContext) getOwner().getContext(getEnvironment());
        assert(context.getEnvironment().canPlayRole(this.rolePlayedByParticipants));
        getOwner().getLogger().fine("Primitive broadcast on environment " + getEnvironment() + " by agent " + getOwner().getName());
        AgentAddress myP2PAddress = context.getMyAddress(RStochasticNetworkPeer.class.getName());
        // ***
        Set<String> nodesNames = new HashSet<>();
        List<AgentAddress> nodesAddresses = new ArrayList<>();
        for (AgentAddress agentAddress : getOwner().getAgentsWithRole(getEnvironment(), this.rolePlayedByParticipants)) {
            A targetAgent = (A) agentAddress.getAgent();
            AgentAddress targetP2Paddress = targetAgent.getAgentAddressIn(getEnvironment(), RStochasticNetworkPeer.class);
            if (!targetP2Paddress.equals(myP2PAddress)) {
                if (this.excludedTargetsByName.contains(targetAgent.getName())) {
                    getOwner().getLogger().fine("Excluding target agent " + targetAgent.getName());
                } else {
                    nodesNames.add(targetAgent.getName());
                    nodesAddresses.add(targetP2Paddress);
                }
            }
        }
        Message<AgentAddress, T_payload> messageToOthers = new Message<>(myP2PAddress, nodesAddresses, this.messagePayload);
        messageToOthers.setType(this.messageType);
        messageToOthers.setCountKey(this.messageCountKey);
        getOwner().getLogger().fine("Broadcast from " + getOwner().getName() + " to " + nodesNames);
        (new AcEmitMessageTowardsNetwork<A>(getEnvironment(), getOwner(), messageToOthers)).execute();
        // ***
        Message<AgentAddress, T_payload> messageToSelf = new Message<>(myP2PAddress, myP2PAddress, this.messagePayload);
        messageToSelf.setType(this.messageType);
        messageToSelf.setCountKey(this.messageCountKey);
        getOwner().getLogger().fine("Immediate delivery to oneself : " + getOwner().getName());
        (new AcHandleMessageOnReception<A>(getEnvironment(), getOwner(), messageToSelf)).execute();
    }

    @Override
    public <T extends Action<A>> T copy() {
        return (T) new AcBroadcastToOthersPlayingRoleAndImmediatelyToOneself<>(
                getEnvironment(),
                getOwner(),
                this.messagePayload,
                this.messageType,
                this.messageCountKey,
                this.rolePlayedByParticipants,
                this.excludedTargetsByName);
    }
}
