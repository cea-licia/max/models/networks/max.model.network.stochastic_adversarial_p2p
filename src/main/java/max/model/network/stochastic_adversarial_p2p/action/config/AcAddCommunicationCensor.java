/*********************************************************************
 * Copyright (c) 2023 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/
package max.model.network.stochastic_adversarial_p2p.action.config;

import max.core.action.Action;
import max.model.network.stochastic_adversarial_p2p.agent.StochasticPeerAgent;
import max.model.network.stochastic_adversarial_p2p.context.StochasticP2PContext;
import max.model.network.stochastic_adversarial_p2p.context.filter.P2PMessageFilter;
import max.model.network.stochastic_adversarial_p2p.role.RStochasticNetworkPeer;

/**
 * Action triggered whenever a peer is reconfigured so that
 * a new communication censor is either added or replaces a previous one.
 *
 * @author Erwan Mahe
 */
public class AcAddCommunicationCensor<A extends StochasticPeerAgent> extends Action<A> {

    private final String inputOrOutput;
    private final String censorCause;

    private final P2PMessageFilter messageFilter;


    /**
     * Default constructor.
     */
    public AcAddCommunicationCensor(final String environment, final A owner, String inputOrOutput, String censorCause, P2PMessageFilter messageFilter) {
        super(environment, RStochasticNetworkPeer.class, owner);
        this.inputOrOutput = inputOrOutput;
        this.censorCause = censorCause;
        this.messageFilter = messageFilter;
    }

    @Override
    public void execute() {
        final StochasticP2PContext context = (StochasticP2PContext) getOwner().getContext(getEnvironment());
        if (this.inputOrOutput == "INPUT") {
            if (context.inputCensors.containsKey(this.censorCause)) {
                this.getOwner().getLogger().warning("replacing previous input censor for cause : " + this.censorCause);
            } else {
                this.getOwner().getLogger().info("setting input censor for cause : " + this.censorCause);
            }
            context.inputCensors.put(this.censorCause, this.messageFilter);
        } else if (this.inputOrOutput == "OUTPUT") {
            if (context.outputCensors.containsKey(this.censorCause)) {
                this.getOwner().getLogger().warning("replacing previous output censor for cause : " + this.censorCause);
            } else {
                this.getOwner().getLogger().info("setting output censor for cause : " + this.censorCause);
            }
            context.outputCensors.put(this.censorCause, this.messageFilter);
        } else {
            this.getOwner().getLogger().severe("trying to add censor neither for INPUT nor for OUTPUT : " + this.inputOrOutput);
        }
    }

    @Override
    public <T extends Action<A>> T copy() {
        return (T) new AcAddCommunicationCensor<>(getEnvironment(), getOwner(), this.inputOrOutput, this.censorCause, this.messageFilter);
    }

}
