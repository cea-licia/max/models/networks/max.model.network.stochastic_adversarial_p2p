/*********************************************************************
 * Copyright (c) 2023 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/
package max.model.network.stochastic_adversarial_p2p.action.config;

import madkit.kernel.AgentAddress;
import max.core.action.Action;
import max.model.network.stochastic_adversarial_p2p.agent.StochasticPeerAgent;
import max.model.network.stochastic_adversarial_p2p.context.StochasticP2PContext;
import max.model.network.stochastic_adversarial_p2p.context.filter.P2PMessageFilter;
import max.model.network.stochastic_adversarial_p2p.role.RStochasticNetworkPeer;
import org.apache.commons.lang3.tuple.Pair;

import java.util.Vector;

/**
 * Action triggered whenever a peer is reconfigured so that
 * a new communication redirection is either added or replaces a previous one.
 *
 * @author Erwan Mahe
 */
public class AcAddCommunicationRedirect<A extends StochasticPeerAgent> extends Action<A> {

    private final String inputOrOutput;
    private final String redirectCause;

    private final P2PMessageFilter messageFilter;

    private final Vector<AgentAddress> listeners;


    /**
     * Default constructor.
     */
    public AcAddCommunicationRedirect(final String environment, final A owner, String inputOrOutput, String redirectCause, P2PMessageFilter messageFilter, Vector<AgentAddress> listeners) {
        super(environment, RStochasticNetworkPeer.class, owner);
        this.inputOrOutput = inputOrOutput;
        this.redirectCause = redirectCause;
        this.messageFilter = messageFilter;
        this.listeners = listeners;
    }

    @Override
    public void execute() {
        final StochasticP2PContext context = (StochasticP2PContext) getOwner().getContext(getEnvironment());
        if (this.inputOrOutput == "INPUT") {
            if (context.inputRedirects.containsKey(this.redirectCause)) {
                this.getOwner().getLogger().warning("replacing previous input redirect for cause : " + this.redirectCause);
            } else {
                this.getOwner().getLogger().info("setting input redirect for cause : " + this.redirectCause);
            }
            context.inputRedirects.put(this.redirectCause, Pair.of(this.messageFilter,this.listeners));
        } else if (this.inputOrOutput == "OUTPUT") {
            if (context.outputRedirects.containsKey(this.redirectCause)) {
                this.getOwner().getLogger().warning("replacing previous output redirect for cause : " + this.redirectCause);
            } else {
                this.getOwner().getLogger().info("setting output redirect for cause : " + this.redirectCause);
            }
            context.outputRedirects.put(this.redirectCause, Pair.of(this.messageFilter,this.listeners));
        } else {
            this.getOwner().getLogger().severe("trying to add redirect neither for INPUT nor for OUTPUT : " + this.inputOrOutput);
        }
    }

    @Override
    public <T extends Action<A>> T copy() {
        return (T) new AcAddCommunicationRedirect<>(getEnvironment(), getOwner(), this.inputOrOutput, this.redirectCause, this.messageFilter, this.listeners);
    }

}
