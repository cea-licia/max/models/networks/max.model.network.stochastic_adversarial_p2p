/*********************************************************************
 * Copyright (c) 2023 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/
package max.model.network.stochastic_adversarial_p2p.agent;

import max.core.action.Plan;
import max.core.agent.SimulatedAgent;
import max.model.network.stochastic_adversarial_p2p.role.RStochasticNetworkPeer;

/**
 * Base agent for any user agent willing to communicate with others during the simulation.
 *
 * Mostly a {@link SimulatedAgent} that can play the {@link RStochasticNetworkPeer} role.
 *
 * @author Erwan Mahe
 */
public class StochasticPeerAgent extends SimulatedAgent {

	/**
	 * Default constructor
	 * 
	 * @param plan Agent's plan
	 */
	public StochasticPeerAgent(final Plan<? extends StochasticPeerAgent> plan) {
		super(plan);
		addPlayableRole(RStochasticNetworkPeer.class);
	}
}
