/*********************************************************************
 * Copyright (c) 2023 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/
package max.model.network.stochastic_adversarial_p2p.env;

import madkit.kernel.AgentAddress;
import max.core.Context;
import max.core.action.ACTakeRole;
import max.core.agent.SimulatedAgent;
import max.core.agent.SimulatedEnvironment;
import max.datatype.com.Message;
import max.model.network.stochastic_adversarial_p2p.agent.StochasticPeerAgent;
import max.model.network.stochastic_adversarial_p2p.context.StochasticP2PContext;
import max.model.network.stochastic_adversarial_p2p.role.RStochasticNetworkPeer;
import max.model.network.stochastic_adversarial_p2p.role.RStochasticP2PEnvironment;

import java.util.*;

/**
 * An environment for exchanging messages that may be:
 * - redirected (e.g., by malicious entities)
 * - censored (e.g., due to a non-reliable network or by malicious entities)
 * - delayed (e.g., due to network issues or by malicious entities)
 *
 * Redirection may target specific senders, receivers and message types and content.
 *
 * Censorship may target specific senders, receivers and message types and content.
 * The occurrence of censorship can be probabilistic (e.g., for unreliable networks).
 *
 * Delays may target specific senders, receivers and message types and content.
 * The concrete delays can be obtained via observation of probability distributions.
 *
 * The environment stores messages that are in transit between agents in queues.
 * Agents join this environment with specific roles (an agent can join with several distinct role).
 * For each couple (agent/role) a dedicated queue (mailbox) stores messages the agent is expected to receive later on.
 * Although the environment can't forbid an agent to join it with an unknown role,
 * no address will be associated to that agent-role couple.
 *
 * When an agent drops a role in the environment, the associated mailbox is immediately destroyed and messages are lost.
 *
 * @author Erwan Mahe
 */
public class StochasticP2PEnvironment extends SimulatedEnvironment {

	/**
	 * Default constructor.
	 */
	public StochasticP2PEnvironment() {
		super();
		addAllowedRole(RStochasticNetworkPeer.class);
	}


	@Override
	protected Context createContext(SimulatedAgent agent) {
		return new StochasticP2PContext((StochasticPeerAgent) agent, this);
	}

	@Override
	protected void activate() {
		super.activate();
		new ACTakeRole<>(getName(), RStochasticP2PEnvironment.class, this).execute();
	}

	/**
	 * Makes a copy of a message but replacing the addresses of the targets/receivers
	 * **/
	public Message<AgentAddress, ?> copyWithNewReceivers(
			Message<AgentAddress, ?> original,
			List<AgentAddress> receivers,
			boolean isListenRedirect) {
		final var copiedMessage = new Message<>(
				original.getSender(),
				receivers,
				original.getPayload()
		);
		copiedMessage.setType(original.getType());
		if (isListenRedirect) {
			copiedMessage.setCountKey(original.getCountKey() + "redirect");
		} else {
			copiedMessage.setCountKey(original.getCountKey());
		}
		return copiedMessage;
	}

}
