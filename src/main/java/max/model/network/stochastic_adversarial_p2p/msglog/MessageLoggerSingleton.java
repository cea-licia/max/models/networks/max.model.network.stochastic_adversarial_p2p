/*********************************************************************
 * Copyright (c) 2023 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/

package max.model.network.stochastic_adversarial_p2p.msglog;


import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;


/**
 * A Singleton that is used to print on a log file messages that are emitted and received from/by a specific agent on the given P2P layer.
 *
 * @author Erwan Mahe
 */
public final class MessageLoggerSingleton {


    // The field must be declared volatile
    private static volatile max.model.network.stochastic_adversarial_p2p.msglog.MessageLoggerSingleton instance;


    public HashMap<
            String,
            PrintWriter
            > writers;

    private MessageLoggerSingleton() {
        this.writers = new HashMap<>();
    }

    public static max.model.network.stochastic_adversarial_p2p.msglog.MessageLoggerSingleton getInstance() {
        // Double-checked locking (DCL) prevents race condition between multiple
        // threads that may attempt to get singleton instance at the same time,
        // creating separate instances as a result.
        //
        // The `result` variable is important here.
        //
        // https://refactoring.guru/fr/design-patterns/singleton
        // https://refactoring.guru/java-dcl-issue
        max.model.network.stochastic_adversarial_p2p.msglog.MessageLoggerSingleton result = instance;
        if (result != null) {
            return result;
        }
        synchronized(max.model.network.stochastic_adversarial_p2p.msglog.MessageLoggerSingleton.class) {
            if (instance == null) {
                instance = new max.model.network.stochastic_adversarial_p2p.msglog.MessageLoggerSingleton();
            }
            return instance;
        }
    }

    public static void resetLogger() {
        MessageLoggerSingleton current_instance = getInstance();
        for (PrintWriter writer : current_instance.writers.values()) {
            writer.print("End Of Simulation");
            writer.close();
        }
        instance = null;
    }

    public void logMessage(
            String pathPrefix,
            String agentName,
            String messageString,
            boolean is_reception) {
        String symbol;
        if (is_reception) {
            symbol = "?";
        } else {
            symbol = "!";
        }
        String toWrite = agentName + symbol + messageString + "\n";
        if (this.writers.containsKey(agentName)) {
            PrintWriter printWriter = this.writers.get(agentName);
            printWriter.write(toWrite);
            printWriter.flush();
        } else {
            String fileName = pathPrefix + agentName + ".txt";
            FileWriter fileWriter = null;
            try {
                fileWriter = new FileWriter(fileName);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
            PrintWriter printWriter = new PrintWriter(fileWriter);
            printWriter.write(toWrite);
            printWriter.flush();
            this.writers.put(agentName,printWriter);
        }
    }

}