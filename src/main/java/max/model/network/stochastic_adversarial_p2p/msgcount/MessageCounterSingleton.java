/*********************************************************************
 * Copyright (c) 2023 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/

package max.model.network.stochastic_adversarial_p2p.msgcount;


import java.util.HashMap;
import java.util.HashSet;


/**
 * A Singleton that is used to count the number of messages that are exchanged.
 *
 * @author Erwan Mahe
 */
public final class MessageCounterSingleton {


    // The field must be declared volatile
    private static volatile MessageCounterSingleton instance;


    // for each environment,
    // for each message count key,
    // keeps track of three kinds of numbers
    // related to the number of times, a message with that count key has been emitted, broadcast, received etc.
    public HashMap<
                        String,
                        HashMap<String,HashMap<MsgCountNature,Integer>>
            > countPerEnvironment;

    // determines the environments in which we want to count messages
    // by default we do not count messages so as to avoid simulation overheads
    private HashSet<String> environmentsInWhichActive;

    private MessageCounterSingleton() {
        this.countPerEnvironment = new HashMap<>();
        this.environmentsInWhichActive = new HashSet<>();
    }

    public void setActive(String environment) {
        this.environmentsInWhichActive.add(environment);
    }

    public static MessageCounterSingleton getInstance() {
        // Double-checked locking (DCL) prevents race condition between multiple
        // threads that may attempt to get singleton instance at the same time,
        // creating separate instances as a result.
        //
        // The `result` variable is important here.
        //
        // https://refactoring.guru/fr/design-patterns/singleton
        // https://refactoring.guru/java-dcl-issue
        MessageCounterSingleton result = instance;
        if (result != null) {
            return result;
        }
        synchronized(MessageCounterSingleton.class) {
            if (instance == null) {
                instance = new MessageCounterSingleton();
            }
            return instance;
        }
    }

    public static void resetCounter() {
        instance = null;
    }

    public void registerMessageCount(
            String environmentName,
            String messageCountKey,
            MsgCountNature nature,
            int count) {
        if (!this.environmentsInWhichActive.contains(environmentName)) {
            return;
        }
        if (!this.countPerEnvironment.containsKey(environmentName)) {
            this.countPerEnvironment.put(environmentName,new HashMap<>());
        }
        HashMap<String,HashMap<MsgCountNature,Integer>> countForEnv = this.countPerEnvironment.get(environmentName);
        if (!countForEnv.containsKey(messageCountKey)) {
            HashMap<MsgCountNature,Integer> countForMsgType = new HashMap<>();
            countForMsgType.put(MsgCountNature.Reception,0);
            countForMsgType.put(MsgCountNature.UniqueEmission,0);
            countForMsgType.put(MsgCountNature.BroadcastEmission,0);
            countForEnv.put(messageCountKey,countForMsgType);
        }
        HashMap<MsgCountNature,Integer> countForMsgType = countForEnv.get(messageCountKey);
        countForMsgType.merge(nature, count, Integer::sum);
        countForEnv.put(messageCountKey,countForMsgType);
        this.countPerEnvironment.put(environmentName,countForEnv);
    }

}