/*********************************************************************
 * Copyright (c) 2023 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/

package max.model.network.stochastic_adversarial_p2p.context.filter;


import madkit.kernel.AgentAddress;
import max.datatype.com.Message;

/**
 * Specifies a filter over messages for potential applications of delays or redirections
 *
 * @author Erwan Mahe
 */
public interface P2PMessageFilter {

    boolean goes_through_the_filter(Message<AgentAddress, ?> message);

}
