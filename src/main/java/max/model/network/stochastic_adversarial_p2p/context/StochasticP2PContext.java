/*********************************************************************
 * Copyright (c) 2023 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/
package max.model.network.stochastic_adversarial_p2p.context;


import madkit.kernel.AgentAddress;
import max.core.Context;
import max.model.network.stochastic_adversarial_p2p.agent.StochasticPeerAgent;
import max.model.network.stochastic_adversarial_p2p.context.delay.DelaySpecification;
import max.model.network.stochastic_adversarial_p2p.context.filter.P2PMessageFilter;
import max.model.network.stochastic_adversarial_p2p.context.handler.IgnoreReceiveHandler;
import max.model.network.stochastic_adversarial_p2p.context.handler.OnReceiveMessageHandler;
import max.model.network.stochastic_adversarial_p2p.env.StochasticP2PEnvironment;
import max.model.network.stochastic_adversarial_p2p.msglog.AbstractMessageLogWriter;
import org.apache.commons.lang3.tuple.Pair;

import java.util.HashMap;
import java.util.Optional;
import java.util.Vector;

/**
 * Context for an agent which may receive and emit messages:
 * - which may be redirected towards listeners/subscribers/spies either when emitted or received
 * - which may be censored either before being send or before being received
 * - which may be delayed either between the sender and the network or between the network and the receiver
 *
 * @author Erwan Mahe
 */
public class StochasticP2PContext extends Context {

	public Optional<Pair<String,AbstractMessageLogWriter>> messageLogWriter;

	/**
	 * Specifies potential redirection of output messages
	 * Whenever this context's owner sends a message, if the message goes through the filter of a redirect,
	 * it is also send to the targets (vector of addresses) of the redirect
	 * **/
	public final HashMap<String,Pair<P2PMessageFilter,Vector<AgentAddress>>> outputRedirects;


	/**
	 * Specifies potential censuring of output messages
	 * Whenever this context's owner sends a message, if the message goes through the filter of a censorship,
	 * it is simply not send
	 * **/
	public final HashMap<String, P2PMessageFilter> outputCensors;


	/**
	 * Specifies delay between emission of the message and it being dispatched to the recipients by the network environment
	 * Each element of the map correspond to a potential cause for delays (e.g., baseline network delay, distance, malicious acts etc.)
	 * The final concrete delay is then obtained from summing concrete delays obtained from each cause.
	 * Specific filters are used to assess whether or not a given message should be affected by a cause.
	 * **/
	public final HashMap<String, Pair<P2PMessageFilter,DelaySpecification> > outputDelays;


	/**
	 * Specifies potential redirection of input messages
	 * Whenever this context's owner receives a message, if the message goes through the filter of a redirect,
	 * it is also forwarded to the targets (vector of addresses) of the redirect
	 * **/
	public final HashMap<String,Pair<P2PMessageFilter,Vector<AgentAddress>>> inputRedirects;


	/**
	 * Specifies potential censuring of input messages
	 * Whenever this context's owner receives a message, if the message goes through the filter of a censorship,
	 * it is simply not received
	 * **/
	public final HashMap<String, P2PMessageFilter> inputCensors;


	/**
	 * Specifies delay between emission of the message and it being dispatched to the recipients by the network environment
	 * Each element of the map correspond to a potential cause for delays (e.g., baseline network delay, distance, malicious acts etc.)
	 * The final concrete delay is then obtained from summing concrete delays obtained from each cause.
	 * Specific filters are used to assess whether or not a given message should be affected by a cause.
	 * **/
	public final HashMap<String, Pair<P2PMessageFilter,DelaySpecification> > inputDelays;


	/**
	 * Specifies how messages should be handled once finally received by the peer.
	 * **/
	private final HashMap<String, OnReceiveMessageHandler> onReceiveHandlers;


	/**
	 * Creates a new instance of {@link StochasticP2PContext}
	 *
	 * @param owner       context's owner
	 * @param environment associated environment
	 */
	public StochasticP2PContext(StochasticPeerAgent owner,
								StochasticP2PEnvironment environment) {
		super(owner, environment);
		this.messageLogWriter = Optional.empty();
		this.outputRedirects = new HashMap<>();
		this.outputCensors = new HashMap<>();
		this.outputDelays = new HashMap<>();
		this.inputRedirects = new HashMap<>();
		this.inputCensors = new HashMap<>();
		this.inputDelays = new HashMap<>();
		this.onReceiveHandlers = new HashMap<>();
	}



	/**
	 * Sets the handler for a specific message type.
	 * This handler will be used whenever a message of that type is received by the peer.
	 * Calling this method replaces the old handler (if it existed), meaning you can override previous behavior.
	 *
	 * @param messageType the message type this handler will be associated to
	 * @param handler     the handler
	 */
	public void setOnReceiveHandlerForMessageType(final String messageType, final OnReceiveMessageHandler handler) {
		if (this.onReceiveHandlers.containsKey(messageType)) {
			this.getOwner().getLogger().warning("substituting previous handler for message type : " + messageType);
		} else {
			this.getOwner().getLogger().info("setup initial handler for message type : " + messageType);
		}
		this.onReceiveHandlers.put(messageType, handler);
	}


	/**
	 * Gets the handler for a specific message type.
	 * This handler will be used whenever a message of that type is received by the peer.
	 *
	 * @param messageType the message type
	 */
	public OnReceiveMessageHandler getOnReceiveHandlerForMessageType(final String messageType) {
		if (!this.onReceiveHandlers.containsKey(messageType)) {
			this.getOwner().getLogger().warning("no handler for message type : " + messageType + " ... ignoring message");
		}
		return this.onReceiveHandlers.getOrDefault(messageType, new IgnoreReceiveHandler());
	}

}
