/*********************************************************************
 * Copyright (c) 2023 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/

package max.model.network.stochastic_adversarial_p2p.context.delay;



/**
 * A fixed delay
 *
 * @author Erwan Mahe
 */
public class FixedDelay implements DelaySpecification {

    private final int fixed_delay;

    public FixedDelay(int fixed_delay) {
        this.fixed_delay = fixed_delay;
    }

    @Override
    public int get_concrete_delay() {
        return this.fixed_delay;
    }

}
