/*********************************************************************
 * Copyright (c) 2023 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/

package max.model.network.stochastic_adversarial_p2p.context.filter;

import madkit.kernel.AgentAddress;
import max.datatype.com.Message;



/**
 * A filter that selects messages at random depending upon a certain level
 * If applied to message loss this signifies
 * that messages may be eliminated at random depending upon a certain reliability level of the network
 *
 * @author Erwan Mahe
 */
public class UniformLevelFilter implements P2PMessageFilter {


    /**
     * A float number between 0 and 1 inclusive
     * When applied to message loss it corresponds to a reliability level for the network and:
     * - 1 signifies a totally reliable network (no message is lost)
     * - 0 signifies a totally unreliable/dead network (all messages are lost)
     * **/
    private final double level;

    public UniformLevelFilter(double level) {
        this.level = level;
    }

    @Override
    public boolean goes_through_the_filter(Message<AgentAddress, ?> message) {
        return ((Math.random() <= this.level));
    }


}
