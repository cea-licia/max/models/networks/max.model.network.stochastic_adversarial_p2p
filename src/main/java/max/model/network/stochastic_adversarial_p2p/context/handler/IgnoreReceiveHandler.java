/*********************************************************************
 * Copyright (c) 2023 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/

package max.model.network.stochastic_adversarial_p2p.context.handler;

import madkit.kernel.AgentAddress;
import max.datatype.com.Message;
import max.model.network.stochastic_adversarial_p2p.context.StochasticP2PContext;


/**
 * A handler that does nothing with a received message
 *
 * @author Erwan Mahe
 */
public class IgnoreReceiveHandler implements OnReceiveMessageHandler {
    @Override
    public void handle(StochasticP2PContext context, Message<AgentAddress, ?> message) {
        // do nothing
    }

}
