/*********************************************************************
 * Copyright (c) 2023 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/

package max.model.network.stochastic_adversarial_p2p.env;


import madkit.kernel.AgentAddress;
import max.datatype.com.Message;
import max.model.network.stochastic_adversarial_p2p.context.StochasticP2PContext;
import max.model.network.stochastic_adversarial_p2p.context.handler.OnReceiveMessageHandler;



/**
 * Basic message handler for test purposes.
 *
 * @author Erwan Mahe
 */
public class BasicTestMessageHandler implements OnReceiveMessageHandler {

    @Override
    public void handle(StochasticP2PContext context, Message<AgentAddress, ?> message) {
        // ***
        String payload = (String) message.getPayload();
        context.getOwner().getLogger().info("agent " + context.getOwner().getName() + " received message " + payload);
    }

}
