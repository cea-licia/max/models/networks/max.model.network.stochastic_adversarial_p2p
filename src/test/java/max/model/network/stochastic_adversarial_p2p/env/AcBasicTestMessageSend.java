/*********************************************************************
 * Copyright (c) 2023 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/

package max.model.network.stochastic_adversarial_p2p.env;

import madkit.kernel.AgentAddress;
import max.core.action.Action;
import max.datatype.com.Message;
import max.model.network.stochastic_adversarial_p2p.action.message.AcEmitMessageTowardsNetwork;
import max.model.network.stochastic_adversarial_p2p.agent.StochasticPeerAgent;
import max.model.network.stochastic_adversarial_p2p.role.RStochasticNetworkPeer;

import java.util.ArrayList;
import java.util.List;


/**
 * Send a basic string message for test purposes.
 *
 * @author Erwan Mahe
 */
public class AcBasicTestMessageSend extends Action<StochasticPeerAgent> {


    private final String message;

    private final String target;


    public AcBasicTestMessageSend(String envName,
                                  StochasticPeerAgent owner,
                                  String message,
                                  String target) {
        super(envName, RStochasticNetworkPeer.class, owner);
        this.message = message;
        this.target = target;
    }

    @Override
    public void execute() {
        // ***
        // retrieve sender information
        AgentAddress myAddress = this.getOwner().getContext(this.getEnvironment()).getMyAddress(RStochasticNetworkPeer.class.getName());
        String myName = myAddress.getAgent().getName();
        // ***
        // retrieve receivers information
        boolean found = false;
        List<AgentAddress> receivers = new ArrayList<>();
        for (AgentAddress agentAddress : this.getOwner().getAgentsWithRole(this.getEnvironment(), RStochasticNetworkPeer.class)) {
            var agentName = agentAddress.getAgent().getName();
            if (agentName.equals(this.target)) {
                receivers.add(agentAddress);
                found = true;
                break;
            }
        }
        if (!found) {
            getOwner().getLogger().warning(
                    "Agent " + myName +
                            " could not find target " +
                            this.target +
                            " to which send the message"
            );
            return;
        }
        // ***
        // create payload and message
        Message<AgentAddress, String> message = new Message<>(
                myAddress,
                receivers,
                this.message);
        message.setType("TestMessage");
        message.setCountKey("TestMessage");
        // ***
        getOwner().getLogger().info(
                "Agent " + myName +
                        " sending message to agent " +
                        this.target
        );
        // ***
        (new AcEmitMessageTowardsNetwork<StochasticPeerAgent>(
                this.getEnvironment(),
                (StochasticPeerAgent) this.getOwner(),
                message)
        ).execute();
    }

    @Override
    public <T extends Action<StochasticPeerAgent>> T copy() {
        return (T) new AcBasicTestMessageSend(
                getEnvironment(),
                getOwner(),
                this.message,
                this.target);
    }
}
