/*********************************************************************
 * Copyright (c) 2023 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/

package max.model.network.stochastic_adversarial_p2p.env;

import static max.core.MAXParameters.clearParameters;
import static max.core.MAXParameters.setParameter;
import static max.core.test.TestMain.launchTester;

import java.math.BigDecimal;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

import max.core.action.ACTakeRole;
import max.core.action.Plan;
import max.core.agent.ExperimenterAgent;
import max.core.agent.MAXAgent;
import max.core.scheduling.ActionActivator;
import max.model.network.stochastic_adversarial_p2p.action.config.AcAddCommunicationDelay;
import max.model.network.stochastic_adversarial_p2p.action.config.AcConfigMessageHandler;
import max.model.network.stochastic_adversarial_p2p.action.config.AcConfigMessageLogger;
import max.model.network.stochastic_adversarial_p2p.agent.StochasticPeerAgent;
import max.model.network.stochastic_adversarial_p2p.context.delay.FixedDelay;
import max.model.network.stochastic_adversarial_p2p.context.filter.UniformLevelFilter;
import max.model.network.stochastic_adversarial_p2p.role.RStochasticNetworkPeer;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInfo;
import org.junit.jupiter.api.io.TempDir;

/**
 * Basic Test in which we simply instantiate 2 peers in an environment.
 *
 * @author Erwan Mahe
 */
public class BasicEmissionReceptionsTest {

  @BeforeEach
  public void before(@TempDir Path tempDir) {
    clearParameters();

    setParameter("MAX_CORE_SIMULATION_STEP", BigDecimal.ONE.toString());
    setParameter("MAX_CORE_UI_MODE", "Silent");
    setParameter("MAX_CORE_RESULTS_FOLDER_NAME", tempDir.toString());
  }

  @Test
  public void testEmissionReceptions(TestInfo testInfo) throws Throwable {

    boolean printLogs = false;

    final var tester =
        new ExperimenterAgent() {

          private StochasticP2PEnvironment env;
            private StochasticPeerAgent peer1;
            private StochasticPeerAgent peer2;

          @Override
          protected List<MAXAgent> setupScenario() {
            final List<MAXAgent> agents = new ArrayList<>();

            env = new StochasticP2PEnvironment();
            agents.add(env);

            peer1 = new StochasticPeerAgent(createPeerPlan(env.getName(), "peer1",2,"hello", "peer2"));
            peer1.setName("peer1");
            agents.add(peer1);

            peer2 = new StochasticPeerAgent(createPeerPlan(env.getName(), "peer2",4,"hi", "peer1"));
            peer2.setName("peer2");
            agents.add(peer2);

            return agents;
          }

          private Plan<StochasticPeerAgent> createPeerPlan(String envName,
                                                           String peerName,
                                                           int tickSent,
                                                           String msg,
                                                           String target) {

            final List<ActionActivator<StochasticPeerAgent>> static_plan = new ArrayList<>();
            final var join_time = getCurrentTick();
            final var send_time = join_time.add(BigDecimal.valueOf(tickSent));
            static_plan.add(new ACTakeRole<StochasticPeerAgent>(envName, RStochasticNetworkPeer.class,null).oneTime(join_time));
            static_plan.add(new AcAddCommunicationDelay<StochasticPeerAgent>(
                    envName,
                    null,
                    "INPUT",
                    "BASELINE",
                    new UniformLevelFilter(1.0),
                    new FixedDelay(10)
            ).oneTime(join_time));
            static_plan.add(
                    new AcConfigMessageHandler<StochasticPeerAgent>(
                            envName,
                            null,
                            "TestMessage",
                            new BasicTestMessageHandler()
                    ).oneTime(join_time)
            );
            if (printLogs) {
              static_plan.add(
                      new AcConfigMessageLogger<StochasticPeerAgent>(
                              envName,
                              null,
                              "C:\\Users\\em244186\\idea_projects\\stochastic_adversarial_p2p\\",
                              new BasicTestMessageLogWriter()
                      ).oneTime(join_time)
              );
            }

            static_plan.add(new AcBasicTestMessageSend(envName,null,msg,target).oneTime(send_time));

            return new Plan<>() {
              @Override
              public List<ActionActivator<StochasticPeerAgent>> getInitialPlan() {
                return static_plan;
              }
            };
          }

        };
    launchTester(tester, 100, testInfo);
  }



}
