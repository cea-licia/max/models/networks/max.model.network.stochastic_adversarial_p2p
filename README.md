# max.model.network.stochastic_adversarial_p2p

This repository contains a model to manage complex P2P message-based communications between agents.

It constitutes a different alternative to the other P2P model in MAX 
([max.model.network.p2p](https://gitlab.com/cea-licia/max/models/networks/max.model.network.p2p)).

In [max.model.network.p2p](https://gitlab.com/cea-licia/max/models/networks/max.model.network.p2p):

- exchanged messages are stored in an attribute of the P2P environment upon their emission by the emitting agent.
A signal is then emitted when the expected reception time (at a given simulation tick) is reached.
Upon receiving that signal, the target agent (target of the reception) then retrieves the message by removing it from
the environment's storage.
- the use of signals to notify reception events to target agents forces the developer to register message handlers 
in the reactive part of the plan of the agents which is cumbersome and cannot be modified in the course of a simulation 
- there is a global delay "MAX_MODEL_NETWORK_DELAY" that is the fixed delay always applied between an emission and a corresponding reception
- there is a global reliability "MAX_MODEL_NETWORK_RELIABILITY" that gives a likelihood (between 0 and 1) for a message to be lost


The goal of the new P2P model is to be able to more finely control message propagation.
For instance, we want to be able to parameterize specific delays between each pairs of agents instead of a global delay.
We may also want to be able to stop some messages to or redirect them towards unexpected targets, reflecting malicious behavior in the network.
We also want this parameterization to be able to change during the run of a given simulation, reflecting dynamic changes in the network, while providing an easy to use interface for developers.




## Basic Principle

In this new P2P model, inspired by the Actor model and communicating state machines,
we rely on the chained execution of atomic actions 
rather than on the environment to propagate messages.


Let us consider the example illustrated below,
where the agent on the left wants to broadcast a message "M" to the two agents on the right.

<img src="./README_images/broadcast_actions_sequence/emit_0.png">


In this new P2P model, we use successive and causally related executions of actions (chain of actions) 
to propagate the message M.

This is illustrated via the four steps of the table below:

|                                                                       |                                                                       |
|-----------------------------------------------------------------------|-----------------------------------------------------------------------|
| 1 : <img src="./README_images/broadcast_actions_sequence/emit_1.png"> | 2 : <img src="./README_images/broadcast_actions_sequence/emit_2.png"> |
| 3 : <img src="./README_images/broadcast_actions_sequence/emit_3.png"> | 4 : <img src="./README_images/broadcast_actions_sequence/emit_4.png"> |

- 0->1: when an agent wants to emit or broadcast a message it executes or schedules an "emit" action that carries the message M.
- 1->2: upon being executed this action schedules the execution of a "dispatch" action that occurs on the network. The message is propagated to this new action by moving it from "emit" to "dispatch".
- 2->3: upon being executed, the "dispatch" action schedules the execution of a number of "forward" actions, one per agent that is targeted by the broadcast. Likewise, the message "M" is propagated by copying and setting the correct attributes of the actions.
- 3->4: upon being executed, a "forward" action schedules the execution of a "handle" action
- 4: finally, the execution of the "handle" action corresponds to the reception of "M" by the agent on which it is executed


## Control points for stochastic and adversarial behavior

This separation of the broadcast into successive actions allows us to have a fine grained control on message propagation
by providing us with many useful control points in which one can:
- stop the propagation
- delay the propagation
- redirect the message towards other targets

We present 2 examples below.


In the first example below, upon being emitted by the agent on the left, we:

- delay the dispatch of "M" thus delaying the receptions for all initial targets of the message
- redirect "M" towards the agent at the top (which was not expected to receive it initially) via scheduling a "forward" action

<img src="./README_images/control_points/attack_output.png">


In the second example below, upon being forwarded to the agent at the bottom, we:

- cancel the reception ("handle") of the message
- redirect "M" towards the agent at the top (which was not expected to receive it initially) via scheduling a "forward" action

<img src="./README_images/control_points/attack_input.png">


## Message Filters, Delay Specifications and Message Handlers


The behaviors above are encoded via setting the correct attributed to the "contexts" of relevant agents as described in the code below:

<img src="./README_images/control_points/p2p_context.png">

During the execution of an "emit" action on a given agent and for a given message "M",
the fields "outputRedirects", "outputCensors" and "outputDelays"
in the context of the emitter agent are used to check:
- if the message "M" must be redirected towards other agents at that moment
- if it muse be censored i.e. its propagation to its intended targets cancelled
- and (if not censored) if its propagation must be delayed and if so, how much time


Likewise, during the execution of a "forward" action on a given agent and for a given message "M",
the fields "inputRedirects", "inputCensors" and "inputDelays"
in the context of the receiving agent are used to check:
- if the message "M" must be redirected towards other agents at that moment
- if it must be censored i.e. this specific target agent will not receive "M"
- and (if not censored) if the effectual handling of the message must be further delayed and if so, how much time


These fields (which are symmetric for inputs and outputes) are further described below:

<img src="./README_images/control_points/p2p_context_inputs.png">

The "String" in the HashMaps of "redirects", "censors" and "delays" indicates a "reason" 
(mainly for logging purposes) for the redirection, censorship or delaying of messages.
For each "reason" for doing something, a P2PMessageFilter filters the types of messages "M" on which this thing must be done
(hence we can e.g., selectively redirect, censor or delay specific kinds of messages).
For "redirects" the "Vector<AgentAddress>" specifies the targets of the redirection.
For "delays" the "DelaySpecification" specifies a means to retrieve the amount of time (in simulation ticks) delay that is supposed to be added.

Via a "DelaySpecification" we can implement stochastic behavior using a random probabilistic distribution.

The "onReceiveHandlers" map message types (described by a "String") to objects that implements the "OnReceiveMessageHandler" interface
and dictate the behavior of an agent upon the reception of a specific type of message.


## Actions

There are 8 actions that can be scheduled and executed at any given time during a simulation:
- 4 configuration actions which correspond to parameterizing the "redirects", "censors", "delays" et "messageHandlers"
of any P2P agent. This configuration can either be made at instant zero in the simulation to parameterize
the default behavior of agents or at any given later time during the simulation.
Thus, this can be used to implement the dynamic behavior of a malicious adversary that corrupts agents 
and/or network communications.
- 4 communication actions that correspond to the 4 aforementioned steps in our description of communications


<img src="./README_images/actions.png">



## An Adversary Model for Distributed Systems

Via this P2P model, we can easily implement an adaptive adversary with the following capabilities:

<img src="./README_images/adversarial_capabilities.png">

We classify adversarial capabilities according to 
7 types of adversarial actions summarized on the image above.

Each kind of adversarial action is described by a small diagram in which we have:
- on the left the agent that is the target of the adversarial action
- on the bottom (with the mask) the adversary that is the author of the adversarial action
- on the right, the other agents that constitute the Distributed System

See [this paper](TODO) for a detailed description.

- "listen" can be performed by the adversary using "AcAddCommunicationRedirect" actions
- "reveal" can be performed by an action that looks into the context of the target agent
- "send" can be performed via "AcEmitMessageTowardsNetwork"
- "stop" can be trivially performed by stopping the target agent
- "skip" can be performed via "AcAddCommunicationCensor"
- "delay" via "AcAddCommunicationRedirect"
- "inject" either via "AcConfigMessageHandler" or via accessing the target agent's context



# Licence

__max.model.network.stochastic_adversarial_p2p__ is released under
the [Eclipse Public Licence 2.0 (EPL 2.0)](https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html)










